set terminal pdf
set output "5.pdf"
set title "Imigracja do USA z  Europy Północnej \n Histogram wybranych danych"
set style fill solid border -1
set style data histograms
set style histogram rowstacked gap 1
set xtics rotate by -30
plot [][0:300000] "dane.dat" u 6:xtic(1) t "Denmark" , "" u 12 t "Netherlands", "" u 13 t "Norway", '' u 14 t"Sweden"
