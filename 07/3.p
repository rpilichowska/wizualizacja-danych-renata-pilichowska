set terminal pdf
set output "3.pdf"
set hidden3d
set isosamples 30
splot [-2:2][-2:2] exp(-(x**2 + y**2))*cos(x/4)*sin(y)*cos(2*(x**2+y**2))
